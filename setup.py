from setuptools import setup

__version__ = None
execfile('src/centpkg/version.py')

setup(
    name="centpkg",
    version=__version__,
    author="Brian Stinson",
    author_email="bstinson@ksu.edu",
    description="CentOS Plugin to rpkg for managing RPM package sources",
    url="http://bitbucket.org/bstinsonmhk/centpkg.git",
    license="GPLv2+",
    package_dir={'': 'src'},
    packages=['centpkg'],
    scripts=['src/bin/centpkg'],
    data_files=[('/etc/rpkg',['src/centpkg.conf']),]
)
